from django.db import models

# Create your models here.

class R_Text(models.Model):
    name_text = models.CharField(max_length=200)
    data_text = models.CharField(max_length=9999999)
    style_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    def __str__(self):
        return self.name_text
