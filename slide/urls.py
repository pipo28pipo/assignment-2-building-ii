from django.conf.urls import url

from . import views

app_name = 'slide'
urlpatterns = [
##### FOR VIEWING #####
    url(r'^$', views.index, name='index'),
    url(r'^draft$', views.quickDraft, name='quickDraft'),
    url(r'^new$', views.newSlide, name='newSlide'),
    url(r'^edit$', views.edit, name='edit'),
    url(r'^show$', views.showList, name='showList'),
    url(r'^upload$', views.upload, name='upload'),
    url(r'^download$', views.download, name='download'),
##### NOT FOR VIEWING #####
    url(r'^store$', views.store, name='store'),
    url(r'^delete$', views.delete, name='delete'),
    url(r'^save$', views.save, name='save'),
    url(r'^uploaded$', views.uploaded, name='uploaded'),
    url(r'^converted$', views.converted, name='converted'),
]
