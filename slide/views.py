from django.shortcuts import render,redirect
from django.http import HttpResponse,HttpResponseRedirect
from django.core.files.storage import default_storage
from .models import R_Text
from django.utils import timezone
import os
import json as simplejson
def index(request):
    return render(request,'index.html',None)

def download(request):
    textfile = open('slide/static/download.rst','w')
    existSlide = R_Text.objects.filter(name_text=request.GET['name'])
    print(existSlide)
    for item in existSlide:
        print('item found')
        textfile.write(item.data_text)
        slide_type = item.style_text
    textfile.close()
    foldername = ''
    for char in request.GET['name']:
        if char == ' ':
            foldername = foldername+'''_'''
        else:
            foldername = foldername+char
    if slide_type == 'CLEAN':
        os.system('rst2html5 slide/static/download.rst > slide/static/'+foldername+'/download.html')
    elif slide_type == 'DECK':
        os.system('rst2html5 --deck-js --pretty-print-code --embed-content slide/static/download.rst > slide/static/'+foldername+'/download.html')
    elif slide_type == 'REVAL':
        os.system('rst2html5 --jquery --reveal-js --pretty-print-code slide/static/download.rst > slide/static/'+foldername+'/download.html')
    elif slide_type == 'BOOTSTRAP':
        os.system('rst2html5 --bootstrap-css --pretty-print-code --jquery --embed-content slide/static/download.rst > slide/static/'+foldername+'/download.html')
    context  = {'name' : request.GET['name'], 'path' : '/static/'+foldername+'/download.html'}
    return render(request,'download.html',context)

def quickDraft(request):
    return render(request,'quick.html',None)

def converted(request):
    textfile = open('slide/static/rst.rst','w')
    if('name' not in request.GET):
        textfile.write(request.GET['text'])    
        slide_type = request.GET['type']
    elif('name' in request.GET):
        existSlide = R_Text.objects.filter(name_text=request.GET['name'])
        print(existSlide)
        for item in existSlide:
            print('item found')
            textfile.write(item.data_text)
            slide_type = item.style_text
        
    textfile.close()  
    if slide_type == 'CLEAN':
        os.system('rst2html5 slide/static/rst.rst > slide/templates/display.html')
    elif slide_type == 'DECK':
        os.system('rst2html5 --deck-js --pretty-print-code --embed-content slide/static/rst.rst > slide/templates/display.html')
    elif slide_type == 'REVAL':
        os.system('rst2html5 --jquery --reveal-js --pretty-print-code slide/static/rst.rst > slide/templates/display.html')
    elif slide_type == 'BOOTSTRAP':
        os.system('rst2html5 --bootstrap-css --pretty-print-code --jquery --embed-content slide/static/rst.rst > slide/templates/display.html')
    list_text = []
    htmlfile = open('slide/templates/display.html','r')
    for text in htmlfile:
        list_text.append(text)
    htmlfile.close()
    htmlfile = open('slide/static/display.html','w')
    for text in list_text:
        htmlfile.write(text)
    htmlfile.close()
    #htmlfile.write("{% load staticfiles %}")
    os.system('rm slide/static/rst.rst')
    slide = R_Text.objects.filter(name_text__icontains='')
    #for item in slide:
    #    print(item.data_text)
    return render(request,'display.html',None)

def newSlide(request):
    slide = R_Text.objects.filter(name_text__icontains='')
    names = []
    for item in slide:
        names.append(item.name_text)
    json_list = simplejson.dumps(names)
    return render(request,'new.html',{'json_list': json_list})


def store(request):
    if(request.POST):
        new_slide = R_Text(name_text=request.POST['name'],data_text=request.POST['text'],style_text=request.POST['type'],pub_date=timezone.now())
        new_slide.save()
    slide = R_Text.objects.filter(name_text__icontains='')
    for item in slide:
        print(item)
    new_name = ''
    for char in request.POST['name']:
        if char == ' ':
            new_name = new_name+'''_'''
        else:
            new_name = new_name+char
    print(new_name)
    os.system('mkdir slide/static/'+new_name)
    return redirect('/show')

def showList(request):
    all_slide = R_Text.objects.filter(name_text__icontains='').order_by('name_text')
    names = []
    for name in all_slide:
        #print(name.name_text)
        names.append(name.name_text)
    #context = { 'slide' : all_slide}
    #context = { 'slide' : ['s','s']}
    json_list = simplejson.dumps(names)
    return render(request,'show.html',{'json_list': json_list})


def delete(request):
    slide = R_Text.objects.filter(name_text=request.GET['name'])
    slide.delete()
    new_name = ''
    for char in request.GET['name']:
        if char == ' ':
            new_name = new_name+'''_'''
        else:
            new_name = new_name+char
    os.system('rm -r slide/static/'+new_name)
    return redirect('/show')

def edit(request):
    slide = R_Text.objects.filter(name_text=request.GET['name'])
    print('haha')
    return render(request,'edit.html',{'slide': slide})

def save(request):
    slide = R_Text.objects.filter(name_text=request.POST['name'])
    for item in slide:
        item.data_text = request.POST['text']
        #print(request.POST['type'])
        item.style_text = request.POST['type']
        item.save()
    return redirect('/show')

def upload(request):
    all_slide = R_Text.objects.filter(name_text__icontains='').order_by('name_text')
    names = []
    for name in all_slide:
        names.append(name.name_text)
    return render(request,'upload.html',{'names': names})   


def uploaded(request):
    new_name = ''
    for char in request.POST['name']:
        if char == ' ':
            new_name = new_name+'''_'''
        else:
            new_name = new_name+char
    path = 'slide/static/'+new_name+'/'
    img_file = request.FILES['input_file']
    file_path = default_storage.save(path, img_file)
    print(request.POST['file_name']+'sdfsadfsdf')
    new_name = ''
    for char in request.POST['file_name']:
        if char == ' ':
            new_name = new_name+'''_'''
        else:
            new_name = new_name+char    
    os.rename(file_path, path+new_name)
    return redirect('/upload')
